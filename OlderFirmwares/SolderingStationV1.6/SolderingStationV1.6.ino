// Inclusions of Logo and Definitions
#include "SolderingRT1_Logo.h"
#include "SolderingRT1_Parameters.h"

// Include Libraries
#include <TFT.h>
//#include <Adafruit_ST7735.h>
#include <SPI.h>
#include <FlexiTimer2.h>
#include <AutoPID.h>
#include "EEPROM.h"


// Create instance for the library
TFT TFTscreen = TFT(DisplayCSPin, DisplayDCPin, DisplayRSTPin);

//Varibles for PID
double TempMeas1, setPoint1, OutputVal1=0;
double TempMeas2, setPoint2, OutputVal2=0;
double KP = KPconst, KI = KIconst, KD = KDconst;
//Create two PID instances
AutoPID PIDch1(&TempMeas1, &setPoint1, &OutputVal1, OUTPUT_MIN, OUTPUT_MAX, KP, KI, KD);
AutoPID PIDch2(&TempMeas2, &setPoint2, &OutputVal2, OUTPUT_MIN, OUTPUT_MAX, KP, KI, KD);

//LCD printing offset and colours
const int Colour1[]={255,255,0}; //RGB
const int Colour2[]={0,255,0}; //RGB
const int Colour3[]={255,255,255}; //RGB
const long unsigned ColourCH1=YELLOW;
const long unsigned ColourCH2=GREEN;
const int ChannelXOffset[]={5,85};

//Variables
int RotaryCounter = 0; 
int InterruptCounterEncoderSW = 0; 
int InterruptCounterS1 = 0;
int InterruptCounterS2 = 0;
long unsigned int TimeInterruptCounter = 0;
unsigned int InMenuState = 0;
unsigned int MenuItem = 1;
unsigned int MenuItemState = 0;
unsigned int ChannelState[] = {1,1};
int aState;
int aLastState;
int aStateSW;
int aLastStateSW;
int EncoderButtonState;
int Button1State = 1;
int Button2State = 1;
unsigned int SetTemperature[] = {SetTemperatureCh1,SetTemperatureCh2};
unsigned int PresetTemperature[] = {PresetTemperature1,PresetTemperature2};
unsigned int MeasuredTemperature[] = {0,0};
unsigned int MeasuredTemperatureADCvalue[] = {0,0};
unsigned int MaxDutyCycle = PresetMaxDutyCycle;
unsigned int TrackingDutyCycleMin [] = {0,0};
unsigned int TrackingDutyCycleMax [] = {0,0};
long unsigned int TrackingDutyCycleTimerStandby [] = {0,0};
unsigned int VinUVLO = PresetVinUVLO;
int ChannelTrackingState = 0;
int AutoOnState = 1;
//int InSetMode = 0;
int SelectedChannel = 0;
int RefreshScreen = 0;
int RefreshScreenTemperature = 0;
int PreviousChannel = 0;
unsigned int InChannelSettings = 0;
unsigned int FirstBoot = 1;
unsigned int VinUVLO_counter = 0;
unsigned int DisplayRefreshCounter = 0;
float InputVoltage = 12.0;
int IncomingChar = 0;
int InternalTemperatureADC = 0;
float InternalTemperature = 0.0;
float Value = 0.0;
//Variables for serial communication
bool DebugMode = 0;
char IncomingCmd[100];
int CmdIndex;

 
void setup() {

	pinMode (EncoderAPin,INPUT_PULLUP);
	pinMode (EncoderBPin,INPUT_PULLUP);
	pinMode (EncoderSWPin,INPUT_PULLUP);
	pinMode (TemperatureSensor1Pin,INPUT);
	pinMode (TemperatureSensor2Pin,INPUT);
	pinMode (InputVoltagePin,INPUT);
	pinMode (InternalTemperaturePin,INPUT);
	pinMode (SwitchButton1Pin,INPUT_PULLUP);
	pinMode (SwitchButton2Pin,INPUT_PULLUP);
	pinMode (Heater1Pin, OUTPUT);
	pinMode (Heater2Pin, OUTPUT);
  pinMode (DebugPin,OUTPUT);

	Serial.begin (115200);   


	//initialize the library
	TFTscreen.begin();

	// clear the screen with a black background
	TFTscreen.background(0, 0, 0);
	TFTscreen.setTextSize(1);
	TFTscreen.stroke(255,255,0);//red,green,blue
	TFTscreen.setCursor(10+XOffset,20+YOffset);
	TFTscreen.println("Soldering RT1");
	TFTscreen.println(" ");
	TFTscreen.setTextSize(3);
	TFTscreen.setCursor(10+XOffset,35+YOffset);
	TFTscreen.println(SwVersion);
	TFTscreen.setTextSize(1);
	TFTscreen.setCursor(30+XOffset,80+YOffset);
	TFTscreen.println(Author);
  if (UserName){TFTscreen.setCursor(30+XOffset,95+YOffset); TFTscreen.print("Property of"); TFTscreen.setCursor(30+XOffset,105+YOffset);TFTscreen.println(User);}
	//delay(1500);
	//TFTscreen.background(0, 0, 0);
	//TFTscreen.drawBitmap(5,5,stationLOGO1,124,47,23432);
	delay(500);
	TFTscreen.background(0, 0, 0);

	FirstBoot = EEPROM.read(EEPROM_address);
	if (FirstBoot==0){
		//It's not the first boot
		//Load default values
		LoadParameters();
		//Check that data are not corrupted
		if ((PresetTemperature[0]<MinimumTemperature)|(PresetTemperature[0]>MaximumTemperature)){
			InitializeEEPROM();
		}
		if ((PresetTemperature[1]<MinimumTemperature)|(PresetTemperature[1]>MaximumTemperature)){
			InitializeEEPROM();
		}
		if (MaxDutyCycle>255){
		  InitializeEEPROM();
		}
		if (VinUVLO>255){
		  InitializeEEPROM();
		}
		//Now everything good, we can set the set-points
		//Load the channels to de default parameters
		SetTemperature[0] = PresetTemperature[0];
		SetTemperature[1] = PresetTemperature[1];
	} else {
		//Write the memory cells, as content is unknown
		InitializeEEPROM();
	}

	//Print Soldering Iron Channels T1 and T2
	PrintChannel(Colour1,ChannelXOffset[0],1);
	PrintChannel(Colour2,ChannelXOffset[1],2);
	PrintPresetTemperatures();

	// Reads the initial state of the EncoderA
	aLastState = digitalRead(EncoderAPin);
	//PWM frequency
	/*  Setting   Divisor   Frequency
	0x01    1     31372.55 //MOSFET gets TOO HOT
	0x02    8     3921.16 //TOO high noise pitch
	0x03      64    490.20   <--DEFAULT
	0x04      256     122.55
	0x05    1024    30.64
	TCCR1B = (TCCR1B & 0b11111000) | <setting>; */
	TCCR1B = (TCCR1B & 0b11111000) | 0x04;
	//Setup interrupt
	FlexiTimer2::set(2, InterruptMain); // Good was : 2 = interrupt runs every 2ms
	FlexiTimer2::start();

	//Ensure Heaters are OFF
	digitalWrite(Heater1Pin,0);
	digitalWrite(Heater2Pin,0);

	//Configure PIDs
	//If temperature is more than 50 degrees below or above setpoint, OUTPUT will be set to min or max respectively
	PIDch1.setBangBang(BangBangHysteresisON,BangBangHysteresisOFF);
	PIDch2.setBangBang(BangBangHysteresisON,BangBangHysteresisOFF);
	//set PID update interval to XXX ms
	PIDch1.setTimeStep(PIDCalculationInterval);
	PIDch2.setTimeStep(PIDCalculationInterval);
	
	delay(100);
	//Read input voltage and calculate max duty
	//InputVoltage = analogRead(InputVoltagePin)*InputVoltageScaling;
}

void loop() {
	delay(LoopDelay); 
	UpdatePWM();
	ExecuteControl();
	ReadInternalTemperature();
	ExecuteStateMachine();
  if (DisplayRefreshCounter >= DisplayRefreshEveryNLoop) {
    UpdateScreen();
    DisplayRefreshCounter = 0;
  } else { DisplayRefreshCounter++; }
	ExecuteSerialCommunication();
	if (DebugMode) { SerialDebug(); }
}


void ExecuteControl(){
	//Monitor Duty Cycle for Standby Function
	MonitorChannels();
	StopPWM();
	delay(DelayMeasurementms);
	//Read input voltage
	InputVoltage = analogRead(InputVoltagePin)*InputVoltageScaling;
	//Read Temperatures
	ReadTemperatures();
	//Update PID setpoints
	setPoint1 = SetTemperature[0];
	setPoint2 = SetTemperature[1];
	//Compute both PIDs and Write Outputs
	UpdatePWM();
}

void ReadInternalTemperature(){
	InternalTemperature = 0.0;
	InternalTemperatureADC = analogRead(InternalTemperaturePin);
	Value = InternalTemperatureADC;
	Value = 1023 / Value - 1;
	Value = NTCSerieResistor / Value;
	InternalTemperature = Value / NTCResistance;     // (R/Ro)
	InternalTemperature = log(InternalTemperature); // ln(R/Ro)
	InternalTemperature /= NTCBeta;                   // 1/B * ln(R/Ro)
	InternalTemperature += 1.0 / (NTCRefTemperature + 273.15); // + (1/To)
	InternalTemperature = 1.0 / InternalTemperature;                 // Invert the value
	InternalTemperature -= 273.15;                         // Convert it to Celsius
}

void StopPWM(){
	analogWrite(Heater1Pin, 0);
	analogWrite(Heater2Pin, 0);
}

void UpdatePWM(){
	//Enable the PWM only if the channel is active
	if (ChannelState[0]==1)  //Checks that a soldering probe is conencted to the station, if not T>500 (ADC saturated)
	{	if (MeasuredTemperature[0]<TemperatureDisconnectSensing) {
			PIDch1.run();
			analogWrite(Heater1Pin, OutputVal1);
		} else {
			ToggleChannelONOFF(1);
			PIDch1.reset();
		}
	} else if ((ChannelState[0]==2)&(MeasuredTemperature[0]<TemperatureDisconnectSensing)) {
			//Channel was disconnected but now is connected
			if (AutoOnState == 1) {
				//change state back to ON (Auto ON function, Default)
				SwitchChannelON(1);
			} else {
				//change state back to OFF
				SwitchChannelOFF(1);
			}
		} else if((ChannelState[0]==0)&(MeasuredTemperature[0]>=TemperatureDisconnectSensing)) 
			{ //The heater has been disconnected
				SwitchChannelNC(1);
			} 
			else {//Disable PID - Reset
				PIDch1.reset();
			}


	//Enable the PWM only if the channel is active
	if (ChannelState[1]==1)  //Checks that a solering probe is conencted to the station, if not T>500 (ADC saturated)
	{	if (MeasuredTemperature[1]<TemperatureDisconnectSensing) {
			PIDch2.run();
			analogWrite(Heater2Pin, OutputVal2);
		} else {
			ToggleChannelONOFF(2);
			PIDch2.reset();
		}
	} else if ((ChannelState[1]==2)&(MeasuredTemperature[1]<TemperatureDisconnectSensing)) {
			//Channel was disconnected but now is connected
			if (AutoOnState == 1) {
				//change state back to ON (Auto ON function, Default)
				SwitchChannelON(2);
			} else {
				//change state back to OFF
				SwitchChannelOFF(2);
			}
		} else if((ChannelState[1]==0)&(MeasuredTemperature[1]>=TemperatureDisconnectSensing))
			{ //The heater has been disconnected
				SwitchChannelNC(2);
			} 
			else {//Disable PID - Reset
				PIDch2.reset();
			}
	
}

void ReadTemperatures(){
	MeasuredTemperatureADCvalue[0] = analogRead(TemperatureSensor1Pin);
	MeasuredTemperatureADCvalue[1] = analogRead(TemperatureSensor2Pin);
	MeasuredTemperature[0] = MeasuredTemperatureADCvalue[0]*ADCtoTemperatureGain+ADCtoTemperatureOffset;
	MeasuredTemperature[1] = MeasuredTemperatureADCvalue[1]*ADCtoTemperatureGain+ADCtoTemperatureOffset;
	if ((TempMeas1!=MeasuredTemperature[0])||(TempMeas2!=MeasuredTemperature[1])) {
		//Update measured temperature Ch.1
		RefreshScreenTemperature = 1;
	}
	TempMeas1 = MeasuredTemperature[0];
	TempMeas2 = MeasuredTemperature[1];
}

void SetChannelTemperature(){
    if ((SelectedChannel>0)&&(InMenuState==0)){
		if (SelectedChannel==1){
			SetTemperature[0] = SetTemperature[0] + RotaryCounter;
			//Check temperature ranges
			if (SetTemperature[0] > MaximumTemperature){
				SetTemperature[0] = MinimumTemperature;
			}
			else if (SetTemperature[0] < MinimumTemperature){
				SetTemperature[0] = MaximumTemperature;
			}
     if (ChannelTrackingState==1) {
        //Set temperature for both channels
        SetTemperature[1] = SetTemperature[0];
     }
		}
		else {
			SetTemperature[1] = SetTemperature[1] + RotaryCounter;
			//Check temperature ranges
			if (SetTemperature[1] > MaximumTemperature){
				SetTemperature[1] = MinimumTemperature;
			}
			else if (SetTemperature[1] < MinimumTemperature){
				SetTemperature[1] = MaximumTemperature;
			}
     if (ChannelTrackingState==1) {
        //Set temperature for both channels
        SetTemperature[0] = SetTemperature[1];
     }
		}
		if (RotaryCounter!=0) {
			// Update screen only if knob is rotated
			PrintSetTemperature(Colour1,ChannelXOffset[0],1);
			PrintSetTemperature(Colour2,ChannelXOffset[1],2);
			RotaryCounter = 0;
		}
    }
}

void SwitchChannelOFF(int Ch){
  if (Ch==1){
    ChannelState[0] = 0;
  } else if (Ch==2){
    ChannelState[1] = 0;
  }
}

void SwitchChannelNC(int Ch){
  if (Ch==1){
    ChannelState[0] = 2;
  } else if (Ch==2){
    ChannelState[1] = 2;
  }
}

void SwitchChannelON(int Ch){
  if (Ch==1){
    ChannelState[0] = 1;
  } else if (Ch==2){
    ChannelState[1] = 1;
  }
}

void ToggleChannelONOFF(int Ch){
  if (Ch==1){
	  if (ChannelState[0]==1){
		  ChannelState[0] = 0;
	  } else if (ChannelState[0]==0) {
		  ChannelState[0] = 1;
	  }
  } else if (Ch==2){
	  if (ChannelState[1]==1){
		  ChannelState[1] = 0;
	  } else if (ChannelState[1]==0) {
		  ChannelState[1] = 1;
	  }
  }
}

void MonitorChannels(){
	TrackingDutyCycleMin[0] = OutputVal1;
	TrackingDutyCycleMin[1] = OutputVal2;
}

void ExecuteStateMachine(){
	if ((SelectedChannel>0)&&(InMenuState==0)){
		//Set temperature via the rotary encoder
		SetChannelTemperature();
		//If rotary knob is pushed and channel is selected and NOT in MENU, toggle channel state (ON/OFF)
		if((SelectedChannel==1)&&(InterruptCounterEncoderSW>ButtonsPressLatch)){
			if (TimeInterruptCounter>0) {
				// Button was released, toggle channel
				ToggleChannelONOFF(1);
				Serial.println("TOGGLE1");
				InterruptCounterEncoderSW = 0;
			} //Else: do nothing 
		}
		if((SelectedChannel==2)&&(InterruptCounterEncoderSW>ButtonsPressLatch)){
			if (TimeInterruptCounter>0) {
				// Button was released, toggle channel
				ToggleChannelONOFF(2);
				Serial.println("TOGGLE2");
				InterruptCounterEncoderSW = 0;
			} //Else: do nothing /wait
		}
	} if ((InMenuState==0)&&(InterruptCounterEncoderSW>ButtonsPressLatch)) {
		//In main screen with no selected channels
		//A short press for the encoder button should not do anything, only LONG press will open the menu
		if (TimeInterruptCounter>0) {
			// Button was released, do nothign but reset counter
			InterruptCounterEncoderSW = 0;
		}
	}
	
	//Handling menu for additional functions
	//NOTE: the soldering station controller keeps running 
	//Check for entering menu
	if ((InterruptCounterEncoderSW>ButtonsLongPress)&&(InMenuState==0)){
		//Enter MENU and print menu for the first time
		InterruptCounterEncoderSW = 0;
		InMenuState = 1;
		PrintMenu();
	}
	//Check if INMENU and handle it
	if (InMenuState == 1){
		//we are in the MENU
		//Delay every menu cycle by 50ms just for visual
		delay(DelayMenuMs);
		//Print Menu
		TFTscreen.setTextColor(WHITE, BLACK);
		TFTscreen.setTextSize(1);
		UpdateMenu();
		//Check for exiting menu
		if (InterruptCounterEncoderSW>ButtonsLongPress){
			//Exit menu, save parameters, update all screen
			InterruptCounterEncoderSW = 0;
			InMenuState = 0;
			//Save parameters
			SaveParameters();
			//Load parameters, ensures all parameters are synch.
			LoadParameters();
			//Refresh screen
			UpdateAllScreen();
			//Exit menu
		}
		else if (InterruptCounterEncoderSW>ButtonsPressLatch){
			if (TimeInterruptCounter>0) {
				// Button was released
				//Go to next item in the menu
				MenuItem++;
				InterruptCounterEncoderSW = 0;
				if (MenuItem>4){
					//Reached last menu item, loop to first
					MenuItem = 1;
				}
			} //Else: do nothing /wait
		}
	}
	
	//Button 1 (LEFT)
	if((Button1State)&&(InterruptCounterS1>0)){
		if(SelectedChannel==1){
		  //VeryLongPress /LongPress /ShortPress
		  if (InterruptCounterS1>ButtonsVeryLongPress){
				//VeryLongPress: Program the Preset Temperature
				PresetTemperature[0] = SetTemperature[0];
				SaveParameters();
				PrintPresetTemperatures();
			} else if (InterruptCounterS1>ButtonsLongPress){
				//LongPress: Load Preset1 on Channel 1
				SetTemperature[0] = PresetTemperature[0];
				RefreshScreen = 1;
			} else {
				//ShortPress
				//Exit channel 1
				SelectedChannel = 0;
				RefreshScreen = 1;
			}
		} else if(SelectedChannel==2) { //No Channel 1 selected
		  //VeryLongPress /LongPress /ShortPress
		  if (InterruptCounterS1>ButtonsVeryLongPress){
				//VeryLongPress: Program the Preset Temperature
				PresetTemperature[0] = SetTemperature[1];
				SaveParameters();
				PrintPresetTemperatures();
			} else if (InterruptCounterS1>ButtonsLongPress){
				//LongPress: Load Preset1 on Channel 1
				SetTemperature[1] = PresetTemperature[0];
				RefreshScreen = 1;
			} else {
				//ShortPress
				//Select channel 1
				SelectedChannel = 1;
				RefreshScreen = 1;
			}
		} else {
			SelectedChannel = 1;
			RefreshScreen = 1;
		}
		InterruptCounterS1 = 0;
	}
  
	//Button 2 (RIGHT)
	if((Button2State)&&(InterruptCounterS2>0)){
		if(SelectedChannel==2){
		  //VeryLongPress /LongPress /ShortPress
		  if (InterruptCounterS2>ButtonsVeryLongPress){
				//VeryLongPress: Program the Preset Temperature
				PresetTemperature[1] = SetTemperature[1];
				SaveParameters();
				PrintPresetTemperatures();
			} else if (InterruptCounterS2>ButtonsLongPress){
				//LongPress: Load Preset2 on Channel 2
				SetTemperature[1] = PresetTemperature[1];
				RefreshScreen = 1;
			} else {
				//ShortPress
				//Exit channel 2
				SelectedChannel = 0;
				RefreshScreen = 1;
			}
		} else if(SelectedChannel==1) { //No Channel 2 selected
		  //VeryLongPress /LongPress /ShortPress
		  if (InterruptCounterS2>ButtonsVeryLongPress){
				//VeryLongPress: Program the Preset Temperature
				PresetTemperature[1] = SetTemperature[0];
				SaveParameters();
				PrintPresetTemperatures();
			} else if (InterruptCounterS2>ButtonsLongPress){
				//LongPress: Load Preset2 on Channel 1
				SetTemperature[0] = PresetTemperature[1];
				RefreshScreen = 1;
			} else {
				//ShortPress
				//Select channel 1
				SelectedChannel = 2;
				RefreshScreen = 1;
			}
		} else {
			SelectedChannel = 2;
			RefreshScreen = 1;
		}
		InterruptCounterS2 = 0;
	}
}

void InterruptMain(void){
	aState = digitalRead(EncoderAPin); // Reads the "current" state of the EncoderA
	// If the previous and the current state of the EncoderA are different, that means a Pulse has occured
	if (aState != aLastState){     
	// If the EncoderB state is different to the EncoderA state, that means the encoder is rotating clockwise
	if (digitalRead(EncoderBPin) != aState) { 
		//RotaryCounter --;
		RotaryCounter = RotaryCounter -RotaryEncoderTemperatureStep;
	} else {
		//RotaryCounter ++;
		RotaryCounter = RotaryCounter +RotaryEncoderTemperatureStep;
		}
	} 
	aLastState = aState; // Updates the previous state of the EncoderA with the current state
	
	//Read buttons
	Button1State = digitalRead(SwitchButton1Pin);
	Button2State = digitalRead(SwitchButton2Pin);
	EncoderButtonState = digitalRead(EncoderSWPin);
	if(!Button1State){
		InterruptCounterS1++;
	}
	if(!Button2State){
		InterruptCounterS2++;
	}
	if (!EncoderButtonState){
		InterruptCounterEncoderSW++;
	}

	//Buttons are active low (they are in pullup)
	if ((Button1State+Button2State+EncoderButtonState)==3){
		//No buttons pressed
		TimeInterruptCounter++;
	} else {
		//If a button is pressed, reset the counter
		TimeInterruptCounter = 0;
	}
  
  /*if (digitalRead(DebugPin)){
    digitalWrite(DebugPin,0);
  } else {
    digitalWrite(DebugPin,1);
  }*/
  
	//Tracking Duty Cycle timer
	//TrackingDutyCycleTimerStandby[0]++;
	//TrackingDutyCycleTimerStandby[1]++;
}
